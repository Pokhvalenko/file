"""file_writer URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path
from apps.filecsv.views import  csv_base, xlsx_base, File, showFile, Update
from apps.users import views as user_views
from . import views
from decouple import config
from django.conf.urls.static import static


DETAIL_URL = config('DETAIL_URL')
SHOW_FILES_URL = config('SHOW_FILES_URL')
SHOW_BASE_URL = config('SHOW_BASE_URL')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.file_writer, name='file_writer'),
    path('login/', auth_views.LoginView.as_view(template_name='users/login.html'), name="login"),
    path('logout/', auth_views.LogoutView.as_view(template_name='users/logout.html'), name="logout"),
    path('register/', user_views.register, name='register'),
    path(DETAIL_URL, File, name="detail"),
    path(SHOW_FILES_URL, showFile, name="show_files"),
    path('update/<id>', Update, name='update'),
    path(SHOW_BASE_URL+'/<id>/', csv_base, name="csv_base"),
    path(config("xlsx_BASE") + '/<id>/', xlsx_base, name="xlsx_base")
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
