from django.shortcuts import render, redirect
from .forms import UserRegisterForm, ProfileForm


def register(request):
    if request.method == 'POST':
        register = UserRegisterForm(request.POST, prefix='register')
        usrprofile = ProfileForm(request.POST, prefix='profile')
        if register.is_valid() * usrprofile.is_valid():
            user = register.save()
            usrprof = usrprofile.save(commit=False)
            usrprof.user = user
            usrprof.set_token()
            usrprof.subscribed = '1'
            usrprof.save()
            return redirect("login")
    else:
        register = UserRegisterForm(prefix='register')
        usrprofile = ProfileForm(prefix='profile')
    return render(request, 'users/register.html', {'userform': register, 'userprofileform': usrprofile})


