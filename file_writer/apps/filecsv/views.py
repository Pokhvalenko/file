from .tasks import first_task, second_task
from django.shortcuts import render, redirect
from datetime import datetime
from django.core.paginator import Paginator
from .models import FileProduct, fileModel, Company
from django.contrib.auth.models import User
from .forms import FileForm, UpdateForm

from django.utils.datastructures import MultiValueDictKeyError


def Update(request, id):
    try:
        if request.method == 'POST':
            csv_or_excel_file = request.FILES['update_file']
            newdoc = fileModel.objects.get(id=id)
            newdoc.file = csv_or_excel_file
            newdoc.updated_at = datetime.now()
            newdoc.save()
            second_task.delay(newdoc.id)
        return render(request, 'updateBase.html')
    except MultiValueDictKeyError:
        return redirect('updateBase.html')


def File(request):
    try:
        if request.method == 'POST':
            form = FileForm(request.POST, request.FILES)
            csv_or_excel_file = request.FILES['file']
            if form.is_valid():
                user_id = form.cleaned_data["user_id"].id
                file = csv_or_excel_file
                path = "http://127.0.0.1:8000/base/" + str(1) + "/"
                updatePath = "http://127.0.0.1:8000/update/" + str(1) + "/"
                newdoc = fileModel(
                    file=file,
                    path=path,
                    update_path = updatePath,
                    user_id=User.objects.get(id=user_id)
                )
                newdoc.save()
                first_task.delay(newdoc.id)

        form = FileForm()
        return render(request, 'show_base.html', {"form": form})
    except MultiValueDictKeyError:
        return redirect('show_base.html')


def showFile(request):
    if request.method == "GET":
        source = fileModel.objects.all()
        return render(request, 'Base[show_file].html', {"source": source})


def csv_base(request, id):
    if request.method == "GET":
        data = FileProduct.objects.filter(file_id=id)
        paginator = Paginator(data, 100)
        page = request.GET.get('page')
        data = paginator.get_page(page)
        return render(request, 'Base.html', {'all_data': data, 'id': id})


def xlsx_base(request, id):
    if request.method == "GET":
        data = Company.objects.filter(base_id=id)
        paginator = Paginator(data, 15)
        page = request.GET.get('page')
        data = paginator.get_page(page)
        return render(request, 'Excel_Base.html', {'all_data': data, 'id': id})
