from django import forms

from .models import fileModel, Company


class FileForm(forms.ModelForm):
    class Meta:
        model = fileModel
        fields = ['file', 'user_id']


class UpdateForm(forms.ModelForm):
    class Meta:
        model = Company
        fields = ['price', 'count', 'product_id']
