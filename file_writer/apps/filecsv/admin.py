from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from .models import Company
from .models import FileProduct, fileModel, Company

admin.site.register(FileProduct)
admin.site.register(fileModel)
admin.site.register(Company)

# Register your models here.
