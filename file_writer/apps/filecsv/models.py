import os

from django.db import models
from django.contrib.auth.models import User
from django.utils.timezone import now


def upload_path(instance, filename):
    return '/'.join(['files', str(instance.file.name)])


class FileProduct(models.Model):
    file_id = models.CharField(max_length=64)
    base_id = models.CharField(max_length=64)
    supplier_code = models.CharField(max_length=64)
    supplier_stock_level = models.CharField(max_length=64)
    supplier_backorder_eta = models.CharField(max_length=64, null=True, blank=True)
    MSKU = models.CharField(max_length=64, null=True, blank=True)
    ASIN = models.CharField(max_length=64, null=True, blank=True)
    UPC = models.CharField(max_length=64)
    MPN = models.CharField(max_length=64, null=True, blank=True)
    supplier_map_price = models.CharField(max_length=64, null=True, blank=True)
    adjusted_supplier_unit_price = models.CharField(max_length=64)
    supplier_unit_price = models.CharField(max_length=64)
    supplier_pack_size = models.CharField(max_length=64, null=True, blank=True)
    supplier_sold_by_increments = models.CharField(max_length=64, null=True, blank=True)
    supplier_case_size = models.CharField(max_length=128, null=True, blank=True)
    supplier_notes = models.CharField(max_length=128, null=True, blank=True)
    supplier_item_description = models.TextField(default="NULL")
    supplier_unit_shipping_weight_in_pounds = models.CharField(max_length=64, null=True, blank=True)
    supplier_status_code = models.CharField(max_length=64, null=True, blank=True)
    misc1 = models.CharField(max_length=256, null=True, blank=True)
    misc2 = models.CharField(max_length=256, null=True, blank=True)
    misc3 = models.CharField(max_length=256, null=True, blank=True)
    misc4 = models.CharField(max_length=256, null=True, blank=True)
    misc5 = models.CharField(max_length=256, null=True, blank=True)
    supplier_sku = models.CharField(max_length=64, null=True, blank=True)
    supplier_upcharge_pct = models.CharField(max_length=64, null=True, blank=True)
    supplier_backorder_qty = models.CharField(max_length=64, null=True, blank=True)
    brand = models.CharField(max_length=64, null=True, blank=True)
    MSRP = models.CharField(max_length=64, null=True, blank=True)
    updated_at = models.CharField(max_length=64, null=True, blank=True)
    supplier_id = models.CharField(max_length=64, null=True, blank=True)
    uploaded_at = models.CharField(max_length=64, null=True, blank=True)
    case_upc = models.CharField(max_length=64, null=True, blank=True)
    category_code = models.CharField(max_length=64, null=True, blank=True)
    shelf_life = models.CharField(max_length=64, null=True, blank=True)

    def __str__(self):
        return f'{self.base_id}'


class fileModel(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=256, default="default.csv")
    file = models.FileField(upload_to=upload_path)
    path = models.CharField(max_length=256, default="file/default.csv")
    update_path = models.CharField(max_length=256, default="file/default.csv")
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    status_id = models.CharField(max_length=256, default="OK")
    created_at = models.DateTimeField(default=now)
    updated_at = models.DateTimeField(auto_now=True)
    type_id = models.CharField(max_length=256, default='none')

    def __str__(self):
        return f'{self.name}'

    def extension(self):
        name, extension = os.path.splitext(self.file.name)
        return extension


class Company(models.Model):
    base_id = models.IntegerField(default=0)
    product_id = models.IntegerField(default=0, blank=True, null=True)
    price = models.IntegerField(default=0, blank=True, null=True)
    count = models.IntegerField(default=0)
