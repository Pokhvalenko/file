from decouple import config
from django.shortcuts import redirect
from file_writer.celery import app
from .assembly import Assembly, Assembly2
from .models import fileModel, Company
import csv
import os
from django.conf import settings
from tablib import Dataset


@app.task
def first_task(id):
    newdoc = fileModel.objects.get(id=id)
    newdoc.type_id = os.path.splitext(str(newdoc.file))[1]
    if os.path.splitext(str(newdoc.file))[1] == ".xlsx":
        dataset = Dataset()
        new_company = newdoc.file
        imported_data = dataset.load(new_company.read(), format='xlsx')
        newdoc.path = newdoc.path = "http://127.0.0.1:8000/" + config("xlsx_BASE") + "/" + str(newdoc.id) + "/"
        newdoc.name = str(newdoc.file)
        newdoc.status_id = settings.STATUSES[0]
        newdoc.save()
        for data in imported_data:
            if data[1]:
                value = Company(
                    product_id=data[0],
                    price=data[1],
                    count=data[2],
                    base_id=id
                )
                value.save()
            newdoc.status_id = settings.STATUSES[1]
            newdoc.save()
    elif not newdoc.file.name.endswith('.csv'):
        newdoc.status_id = settings.STATUSES[2]
        newdoc.save()
    else:
        newdoc.name = newdoc.file.name
        newdoc.status_id = settings.STATUSES[0]
        newdoc.path = "http://127.0.0.1:8000/" + config("SHOW_BASE_URL")+"/" + str(newdoc.id) + "/"
        newdoc.save()
        with open(os.path.join('file_writer', 'media/', str(newdoc.file))) as csvfile:
            reader = csv.DictReader(csvfile)
            Assembly(reader, newdoc)
@app.task
def second_task(id):
    newdoc = fileModel.objects.get(id=id)
    if os.path.splitext(str(newdoc.file))[1] == ".xlsx":
        dataset = Dataset()
        new_company = newdoc.file
        imported_data = dataset.load(new_company.read(), format='xlsx')
        newdoc.status_id = settings.STATUSES[0]
        newdoc.save()
        for data in imported_data:
            if data[1]:
                value = Company.objects.get(product_id=data[0])
                value.price=data[1]
                value.count=data[2]
                value.base_id=id
                value.save()
            newdoc.status_id = settings.STATUSES[1]
            newdoc.save()
    elif not newdoc.file.name.endswith('.csv'):
        newdoc.status_id = settings.STATUSES[2]
        newdoc.save()
    else:
        with open(os.path.join('file_writer', 'media/', str(newdoc.file))) as csvfile:
            reader = csv.DictReader(csvfile)
            Assembly2(reader, newdoc)

