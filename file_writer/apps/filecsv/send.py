from apps.filecsv.models import FileProduct


def Send(newdoc, supplier_backorder_eta, supplier_stock_level,
         supplier_code, MSKU, ASIN, UPC, MPN, supplier_map_price,
         adjusted_supplier_unit_price, supplier_unit_price, supplier_pack_size, supplier_sold_by_increments, supplier_case_size,
         supplier_notes, supplier_item_description, supplier_unit_shipping_weight_in_pounds, supplier_status_code,
         misc1, misc2, misc3, misc4, misc5, supplier_sku, supplier_upcharge_pct, supplier_backorder_qty, brand, MSRP, updated_at, supplier_id,
         uploaded_at, case_upc, category_code, shelf_life, id):
    new_file_example = FileProduct(
        file_id=newdoc.id,
        base_id=id,
        supplier_backorder_eta=supplier_backorder_eta, supplier_stock_level=supplier_stock_level,
        supplier_code=supplier_code,
        MSKU=MSKU, ASIN=ASIN, UPC=UPC, MPN=MPN,
        supplier_map_price=supplier_map_price,
        adjusted_supplier_unit_price=adjusted_supplier_unit_price,
        supplier_unit_price=supplier_unit_price, supplier_pack_size=supplier_pack_size,
        supplier_sold_by_increments=supplier_sold_by_increments, supplier_case_size=supplier_case_size,
        supplier_notes=supplier_notes, supplier_item_description=supplier_item_description,
        supplier_unit_shipping_weight_in_pounds=supplier_unit_shipping_weight_in_pounds,
        supplier_status_code=supplier_status_code, misc1=misc1, misc2=misc2,
        misc3=misc3, misc4=misc4, misc5=misc5, supplier_sku=supplier_sku,
        supplier_upcharge_pct=supplier_upcharge_pct, supplier_backorder_qty=supplier_backorder_qty,
        brand=brand, MSRP=MSRP,
        updated_at=updated_at, supplier_id=supplier_id, uploaded_at=uploaded_at, case_upc=case_upc,
        category_code=category_code, shelf_life=shelf_life
    )
    new_file_example.save()
def Send2(newdoc, supplier_backorder_eta, supplier_stock_level,
         supplier_code, MSKU, ASIN, UPC, MPN, supplier_map_price,
         adjusted_supplier_unit_price, supplier_unit_price, supplier_pack_size, supplier_sold_by_increments, supplier_case_size,
         supplier_notes, supplier_item_description, supplier_unit_shipping_weight_in_pounds, supplier_status_code,
         misc1, misc2, misc3, misc4, misc5, supplier_sku, supplier_upcharge_pct, supplier_backorder_qty, brand, MSRP, updated_at, supplier_id,
         uploaded_at, case_upc, category_code, shelf_life, id):
        file_example = FileProduct.objects.get(id=id)
        file_example.file_id=newdoc.id,
        file_example.base_id=id,
        file_example.supplier_backorder_eta=supplier_backorder_eta,
        file_example.supplier_stock_level=supplier_stock_level,
        file_example.supplier_code=supplier_code,
        file_example.MSKU=MSKU, file_example.ASIN=ASIN, file_example.UPC=UPC, file_example.MPN=MPN,
        file_example.supplier_map_price=supplier_map_price,
        file_example.adjusted_supplier_unit_price=adjusted_supplier_unit_price,
        file_example.supplier_unit_price=supplier_unit_price, file_example.supplier_pack_size=supplier_pack_size,
        file_example.supplier_sold_by_increments=supplier_sold_by_increments, file_example.supplier_case_size=supplier_case_size,
        file_example.supplier_notes=supplier_notes, file_example.supplier_item_description=supplier_item_description,
        file_example.supplier_unit_shipping_weight_in_pounds=supplier_unit_shipping_weight_in_pounds,
        file_example.supplier_status_code=supplier_status_code, file_example.misc1=misc1, file_example.misc2=misc2,
        file_example.misc3=misc3, file_example.misc4=misc4, file_example.misc5=misc5, file_example.supplier_sku=supplier_sku,
        file_example.supplier_upcharge_pct=supplier_upcharge_pct, file_example.supplier_backorder_qty=supplier_backorder_qty,
        file_example.brand=brand, file_example.MSRP=MSRP,
        file_example.updated_at=updated_at, file_example.supplier_id=supplier_id, file_example.uploaded_at=uploaded_at, file_example.case_upc=case_upc,
        file_example.category_code=category_code, file_example.shelf_life=shelf_life
        file_example.save()