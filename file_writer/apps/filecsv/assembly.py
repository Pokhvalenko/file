from apps.filecsv.send import Send, Send2
from django.conf import settings

def Assembly(reader, newdoc):
    for row in reader:
        id = row['id']
        supplier_code = row['supplier_code']
        if row['supplier_stock_level'].replace('.', '', 1).isdigit():
            supplier_stock_level = row['supplier_stock_level']
        else:
            supplier_stock_level = "have to be number!"
        supplier_backorder_eta = row['supplier_backorder_eta']
        MSKU = row['MSKU']
        ASIN = row['ASIN']
        if row['UPC'].replace('.', '', 1).isdigit():
            UPC = row['UPC']
        else:
            UPC = "have to be number!"
        MPN = row['MPN']
        supplier_map_price = row['supplier_map_price']
        if row['adjusted_supplier_unit_price'].replace('.', '', 1).isdigit():
            adjusted_supplier_unit_price = row['adjusted_supplier_unit_price']
        else:
            adjusted_supplier_unit_price = "have to be number!"
        if row['supplier_unit_price'].replace('.', '', 1).isdigit():
            supplier_unit_price = row['supplier_unit_price']
        else:
            supplier_unit_price = "have to be number!"
        if row['supplier_pack_size'].replace('.', '', 1).isdigit():
            supplier_pack_size = row['supplier_pack_size']
        else:
            supplier_pack_size = "have to be number!"
        if row['supplier_sold_by_increments'].replace('.', '', 1).isdigit():
            supplier_sold_by_increments = row['supplier_sold_by_increments']
        else:
            supplier_sold_by_increments = "have to be number!"
        if row['supplier_case_size'].replace('.', '', 1).isdigit():
            supplier_case_size = row['supplier_case_size']
        else:
            supplier_case_size = "have to be number!"
        supplier_notes = row['supplier_notes']
        supplier_item_description = row['supplier_item_description']
        supplier_unit_shipping_weight_in_pounds = row['supplier_unit_shipping_weight_in_pounds']
        supplier_status_code = row['supplier_status_code']
        misc1 = row['misc1']
        misc2 = row['misc2']
        misc3 = row['misc3']
        misc4 = row['misc4']
        misc5 = row['misc5']
        if row['supplier_sku'] is None or row['supplier_sku'].replace('.', '', 1).isdigit():
            supplier_sku = row['supplier_sku']
        else:
            supplier_sku = "have to be number!"
        supplier_upcharge_pct = row['supplier_upcharge_pct']
        supplier_backorder_qty = row['supplier_backorder_qty']
        brand = row['brand']
        if row['MSRP'] is None or row['MSRP'].replace('.', '', 1).isdigit():
            MSRP = row['MSRP']
        else:
            supplier_sku = "have to be number!"
        updated_at = row['updated_at']
        supplier_id = row['supplier_id']
        uploaded_at = row['uploaded_at']
        case_upc = row['case_upc']
        category_code = row['category_code']
        shelf_life = row['shelf_life']
        Send(newdoc, supplier_backorder_eta, supplier_stock_level,
             supplier_code, MSKU, ASIN, UPC, MPN, supplier_map_price,
             adjusted_supplier_unit_price, supplier_unit_price, supplier_pack_size, supplier_sold_by_increments,
             supplier_case_size,
             supplier_notes, supplier_item_description, supplier_unit_shipping_weight_in_pounds, supplier_status_code,
             misc1, misc2, misc3, misc4, misc5, supplier_sku, supplier_upcharge_pct, supplier_backorder_qty, brand,
             MSRP, updated_at, supplier_id,
             uploaded_at, case_upc, category_code, shelf_life, id)
    newdoc.status_id = settings.STATUSES[1]
    newdoc.save()


def Assembly2(reader, newdoc):
    for row in reader:
        id = row['id']
        supplier_code = row['supplier_code']
        if row['supplier_stock_level'].replace('.', '', 1).isdigit():
            supplier_stock_level = row['supplier_stock_level']
        else:
            supplier_stock_level = "have to be number!"
        supplier_backorder_eta = row['supplier_backorder_eta']
        MSKU = row['MSKU']
        ASIN = row['ASIN']
        if row['UPC'].replace('.', '', 1).isdigit():
            UPC = row['UPC']
        else:
            UPC = "have to be number!"
        MPN = row['MPN']
        supplier_map_price = row['supplier_map_price']
        if row['adjusted_supplier_unit_price'].replace('.', '', 1).isdigit():
            adjusted_supplier_unit_price = row['adjusted_supplier_unit_price']
        else:
            adjusted_supplier_unit_price = "have to be number!"
        if row['supplier_unit_price'].replace('.', '', 1).isdigit():
            supplier_unit_price = row['supplier_unit_price']
        else:
            supplier_unit_price = "have to be number!"
        if row['supplier_pack_size'].replace('.', '', 1).isdigit():
            supplier_pack_size = row['supplier_pack_size']
        else:
            supplier_pack_size = "have to be number!"
        if row['supplier_sold_by_increments'].replace('.', '', 1).isdigit():
            supplier_sold_by_increments = row['supplier_sold_by_increments']
        else:
            supplier_sold_by_increments = "have to be number!"
        if row['supplier_case_size'].replace('.', '', 1).isdigit():
            supplier_case_size = row['supplier_case_size']
        else:
            supplier_case_size = "have to be number!"
        supplier_notes = row['supplier_notes']
        supplier_item_description = row['supplier_item_description']
        supplier_unit_shipping_weight_in_pounds = row['supplier_unit_shipping_weight_in_pounds']
        supplier_status_code = row['supplier_status_code']
        misc1 = row['misc1']
        misc2 = row['misc2']
        misc3 = row['misc3']
        misc4 = row['misc4']
        misc5 = row['misc5']
        if row['supplier_sku'] is None or row['supplier_sku'].replace('.', '', 1).isdigit():
            supplier_sku = row['supplier_sku']
        else:
            supplier_sku = "have to be number!"
        supplier_upcharge_pct = row['supplier_upcharge_pct']
        supplier_backorder_qty = row['supplier_backorder_qty']
        brand = row['brand']
        if row['MSRP'] is None or row['MSRP'].replace('.', '', 1).isdigit():
            MSRP = row['MSRP']
        else:
            supplier_sku = "have to be number!"
        updated_at = row['updated_at']
        supplier_id = row['supplier_id']
        uploaded_at = row['uploaded_at']
        case_upc = row['case_upc']
        category_code = row['category_code']
        shelf_life = row['shelf_life']
        Send2(newdoc, supplier_backorder_eta, supplier_stock_level,
             supplier_code, MSKU, ASIN, UPC, MPN, supplier_map_price,
             adjusted_supplier_unit_price, supplier_unit_price, supplier_pack_size, supplier_sold_by_increments,
             supplier_case_size,
             supplier_notes, supplier_item_description, supplier_unit_shipping_weight_in_pounds, supplier_status_code,
             misc1, misc2, misc3, misc4, misc5, supplier_sku, supplier_upcharge_pct, supplier_backorder_qty, brand,
             MSRP, updated_at, supplier_id,
             uploaded_at, case_upc, category_code, shelf_life, id)
    newdoc.status_id = settings.STATUSES[1]
    newdoc.save()